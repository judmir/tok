app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
	//sidebar
		.state('wordpress', {
			url: "/wordpress",
			abstract: true,
			templateUrl: "wordpress/sidebar-menu.html"
		})
		// Blog page
		.state('wordpress.blog', {
			url: "/blog",
			views: {
				'menuWorPress': {
					templateUrl: "wordpress/blog.html",
					controller: "WordpressBlogCtrl"
				}
			}
		})
		.state('wordpress.tag', {
			url: "/tag/:type/:slug",
			views: {
				'menuWorPress': {
					templateUrl: "wordpress/blog.html",
					controller: "WordpressTagCtrl"
				}
			}
		})
		// articles page wordpress
		.state('wordpress.post', {
			url: "/post",
			cache: false,
			views: {
				'menuWorPress': {
					templateUrl: "wordpress/post.html",
					controller: "WordpressPostCtrl"
				}
			}
		})
		// categories page wordpress
		.state('wordpress.categories', {
			url: "/categories",
			views: {
				'menuWorPress': {
					templateUrl: "wordpress/categories.html",
					controller: "WordpressCategoriesCtrl"
				}
			}
		})
		// tags page wordpress
		.state('wordpress.tags', {
			url: "/tags",
			views: {
				'menuWorPress': {
					templateUrl: "wordpress/tags.html",
					controller: "WordpressTagsCtrl"
				}
			}
		});

		$urlRouterProvider.otherwise('/app/dashboard');
});