app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('app.students', {
			url: "/students",
			cache: true,
			views: {
				'menuContent': {
					templateUrl: "staff/students.html",
					controller: "StudentsCtrl"
				}
			}
		})
		.state('app.stuff', {
			url: "/stuff",
			views: {
				'menuContent': {
					templateUrl: "staff/staff.html",
					controller: "StaffCtrl"
				}
			}
		})
		.state('app.viewProfile', {
			url: "/view_profile/:profile_id/:profile_type",
			views: {
				'menuContent': {
					templateUrl: "general/view_profile.html",
					controller: "ViewProfileCtrl"
				}
			}
		})
		.state('app.marks', {
			url: "/marks",
			cache: false,
			views: {
				'menuContent': {
					templateUrl: "staff/marks.html",
					controller: "MarksCtrl"
				}
			}
		})
		.state('app.list-assignments',{
			url: "/list-assignment",
			cache: false,
			views: {
				'menuContent': {
					templateUrl: "staff/assignment_list.html",
					controller: 'EditAssignmentListCtrl'
				}
			}
		})
		.state('app.confirm', {
			url: "/marks/confirm",
			views: {
				'menuContent': {
					templateUrl: "staff/confirm-marks.html",
					controller: "ConfirmMarksCtrl"
				}
			}
		})
		.state('app.list',{
			url: "/marks/:class_id/:course_id",
			cache: false,
			views: {
				'menuContent': {
					templateUrl: "staff/confirm-marks-list.html",
					controller: "ConfirmMarksListCtrl"
				}
			}
		})
		.state('app.list-vcd-assignments',{
			url: "/list-vcd-assignment",
			cache: false,
			views: {
				'menuContent': {
					templateUrl: "staff/assignment_vcd_list.html",
					controller: 'EditVcdAssignmentListCtrl'
				}
			}
		});

		$urlRouterProvider.otherwise('/app/dashboard');
});