/**
 * Change Staff password
 */
app.controller('ChangeStaffPasswordCtrl', function ($scope, $http, $state, $ionicViewService, $ionicLoading, $log, $ionicPopup, ChangeStaffPassword) {

	$scope.closeModalStaffPassword = function () {
		modalChangeStaffPassword.hide();
	};

	/**
	 * Function changeStaffPassword , change staff password
	 * @param passwordData
	 */
	$scope.changeStaffPassword = function (passwordData) {
		if (passwordData.new_password == '') {
			$ionicLoading.show({template: 'Please enter new password', duration: 1500});
		} else {
			// show loader
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			ChangeStaffPassword.response(idToken, userId, staffId, passwordData.new_password)
				.success(function (data) {
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					if (data.status == 'success') {
						modalChangeStaffPassword.hide();
					}
					$ionicLoading.show({template: data.message, duration: 1500});

					console.group('Change Student Password Section');
					$log.log(data);
					console.groupEnd();
				})
				.error(function (error) {
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: 'Something went wrong!',
						template: error
					});
				});
		}
	}
});

/**
 * Students Controller
 *
 * Get all students , on scroll down load more
 */
app.controller('StudentsCtrl', function ($scope, $ionicLoading, $state, $log, $ionicNavBarDelegate, StudentsData, StudentsActive) {

	// set data
	$scope.items = [];
	$scope.times = 1;
	$scope.postsCompleted = false;
	$scope.shouldShowDelete = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe = true;

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	/**
	 * Get all students
	 */
	$scope.getStudentsItem = function () {
		StudentsData.getStudents(idToken, userId, $scope.times)
			.success(function (data) {
				$scope.items = $scope.items.concat(data.results);
				$scope.path = data.urlPath;
				$scope.$broadcast('scroll.infiniteScrollComplete');
				$scope.times = $scope.times + 1;
				if (data.more == 'false' || data.more == false || !data.more) {
					$scope.postsCompleted = true;
				}

				console.group('Show all students Section');
				$log.log(data);
				console.groupEnd();
			})
			.error(function (error) {
				$scope.items = [];
			});
	};

	/**
	 * Get all searched students
	 */
	var doSearch = ionic.debounce(function (name) {
		$scope.times = 1;
		$scope.items = [];
		StudentsData.getStudents(idToken, userId, $scope.times, name)
			.success(function (data) {

				$scope.items = $scope.items.concat(data.results);
				$scope.path = data.urlPath;
				$scope.$broadcast('scroll.infiniteScrollComplete');
				$scope.times = $scope.times + 1;

				if ($scope.times >= data.total) {
					$scope.postsCompleted = true;
				}
				if (data.more == 'false' || data.more == false || !data.more) {
					$scope.postsCompleted = true;
				}

				console.group('Searched students Section');
				$log.log(data);
				console.groupEnd();
			})
			.error(function (error) {
				$scope.items = [];
			});
	}, 500);
	$scope.filter = {};


	$scope.search = function () {
		doSearch($scope.filter.name); // call doSearch to get all searched students
	};

	// pull to refresh buttons
	$scope.doRefresh = function () {
		$scope.times = 1;
		$scope.items = [];
		$scope.postsCompleted = false;
		$state.go($state.current, {}, {reload: true});
		$scope.$broadcast('scroll.refreshComplete');
	};

	/**
	 * Change state of student active/disable
	 */
	$scope.activeStudent = function (student_id, state) {
		StudentsActive.active(idToken, userId, student_id, state)
			.success(function (data) {
				$scope.times = 1;
				$scope.items = [];
				$scope.postsCompleted = false;
				$scope.$broadcast('scroll.refreshComplete');
				$state.go($state.current, {}, {reload: true});
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});
			});
	};
});

/**
 * Students Controller
 *
 * Get all students , on scroll down load more
 */
app.controller('StaffCtrl', function ($scope, $ionicLoading, $log, $state, $ionicNavBarDelegate, StaffData, StaffActive) {
	// set data
	$scope.items = [];
	$scope.times = 1;
	$scope.postsCompleted = false;
	$scope.shouldShowDelete = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe = true;

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	/**
	 * Get all staff
	 */
	$scope.getStaffItem = function () {
		StaffData.getStaff(idToken, userId, $scope.times)
			.success(function (data) {
				$scope.items = $scope.items.concat(data.results);
				$scope.path = data.urlPath;
				$scope.$broadcast('scroll.infiniteScrollComplete');
				$scope.times = $scope.times + 1;
				if (data.more == 'false' || data.more == false || !data.more) {
					$scope.postsCompleted = true;
				}

				console.group('Show all Staff Section');
				$log.log(data);
				console.groupEnd();
			})
			.error(function (error) {
				$scope.items = [];
			});
	};

	/**
	 * Get all searched staff
	 */
	var doSearch = ionic.debounce(function (name) {
		$scope.times = 1;
		$scope.items = [];
		StaffData.getStaff(idToken, userId, $scope.times, name)
			.success(function (data) {

				$scope.items = $scope.items.concat(data.results);
				$scope.path = data.urlPath;
				$scope.$broadcast('scroll.infiniteScrollComplete');
				$scope.times = $scope.times + 1;

				if ($scope.times >= data.total) {
					$scope.postsCompleted = true;
				}
				if (data.more == 'false' || data.more == false || !data.more) {
					$scope.postsCompleted = true;
				}

				console.group('Search Staff Section');
				$log.log(data);
				console.groupEnd();
			})
			.error(function (error) {
				$scope.items = [];
			});
	}, 500);
	$scope.filter = {};


	$scope.search = function () {
		doSearch($scope.filter.name); // call doSearch to get all searched students
	};

	// pull to refresh buttons
	$scope.doRefresh = function () {
		$scope.times = 1;
		$scope.items = [];
		$scope.postsCompleted = false;
		$state.go($state.current, {}, {reload: true});
		$scope.$broadcast('scroll.refreshComplete');
	};

	/**
	 * Change state of student active/disable
	 */
	$scope.activeStaff = function (staff_id, state) {
		StaffActive.active(idToken, userId, staff_id, state)
			.success(function (data) {
				$scope.times = 1;
				$scope.items = [];
				$scope.postsCompleted = false;
				$state.go($state.current, {}, {reload: true});
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});
			});
	};

});

/**
 * View current Staff profile Controller
 *
 * Show staff profile
 * */
app.controller('ViewProfileCtrl', function ($scope, $stateParams, $cordovaCamera, $cordovaFile, $ionicActionSheet, $log, $ionicLoading, $ionicPopup, $ionicModal, ViewProfileData, Config) {

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	/**
	 * Get profile data
	 */
	ViewProfileData.profileData(idToken, userId, $stateParams.profile_id, $stateParams.profile_type)
		.success(function (data) {
			//hide loader
			$ionicLoading.hide();
			$scope.profileData = data;
			$scope.profileName = data.name;
			$scope.profileSurname = data.surname;
			$scope.photo = Config.ApiTokUrl + Config.StuffProfileImgUrl + data.photo;
			$scope.$root.profileData = data;
			$scope.$root.photo = Config.ApiTokUrl + Config.StuffProfileImgUrl + data.photo;

			console.group('View Profile Section');
			$log.log(data);
			console.groupEnd();
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});
});

/**
 * View Marks data
 */
app.controller('MarksCtrl', function ($scope, $stateParams, $state, $cordovaCamera, $cordovaFile, $ionicActionSheet, $log, $ionicNavBarDelegate, $ionicLoading, $ionicPopup, $ionicModal, $ionicPopover, GetMarksClass, GetClassData, SendClassMarks, Config) {

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	/**
	 * Get marks data
	 *
	 */
	GetMarksClass.response(idToken, userId)
		.success(function (data) {

			if(data.classes.length != 0){
				$scope.classes = data.classes;
				$scope.classDefault = data.classes[0];
				var default_class_id = data.classes[0].clsID;
				var default_course_id = data.classes[0].coursesID;
				class_id = data.classes[0].clsID;
				course_id = data.classes[0].coursesID;
				/**
				 * Get default Class Data
				 */
				GetClassData.response(idToken, userId, default_class_id, default_course_id)
					.success(function (data) {
						if (data) {
							$scope.students = data.students;
							$scope.assaignments = data.assaignments;
							$scope.lassaignments = data.lassaignments;
							$scope.marks = data.marks;
							// $log data
							console.group('Class Data');
							$log.log('Students Data: '+$scope.students);
							$log.log('Classes Data: '+$scope.classes);
							$log.log('Assaignments Data: '+$scope.assaignments);
							$log.log('Lassaignments Data: '+$scope.lassaignments);
							$log.log('Marks Data: '+$scope.marks);
							console.groupEnd();
						} else{
							// alert notification where something went wrong
							var alertPopup = $ionicPopup.alert({
								title: '!',
								template: error
							});
						}
						//hide loader
						$ionicLoading.hide();
					})
					.error(function (error) {
						//hide loader
						$ionicLoading.hide();
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
			} else {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Classes was not founded',
					template: 'Sorry! You are not a class teacher.'
				});

				$state.go('app.dashboard');
			}


		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	/**
	 * Get class data on selecting class
	 *
	 * @param data
	 */
	$scope.getClassData = function(data) {
		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		class_id = data.clsID;
		course_id = data.coursesID;
		GetClassData.response(idToken, userId, class_id, course_id)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.students = data.students;
				$scope.assaignments = data.assaignments;
				$scope.lassaignments = data.lassaignments;
				$scope.marks = data.marks;
				$log.log(data.lassaignments);

				console.group('Get Class Data Section');
				$log.log(data);
				console.groupEnd();
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};
	/**
	 * Send student marks
	 *
	 * @param data
	 */
	$scope.sendMarks = function (data) {
		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		console.group('Send Marks Data Section');
		$log.log('Test: '+ JSON.stringify(data));
		console.groupEnd();
		var obj = JSON.stringify(data);
		SendClassMarks.response(idToken, userId, obj)
			.success(function (res) {
				//hide loader
				$ionicLoading.hide();
				$log.log('Result on sending Marks'+res);
				$ionicLoading.show({template: res.message, duration: 1500});
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$ionicPopover.fromTemplateUrl('templates/general/popover/marks_popover.html', {
		scope: $scope
	}).then(function(popover) {
		$scope.popover = popover;
	});

	document.body.classList.remove('platform-ios');
	document.body.classList.remove('platform-android');
	document.body.classList.add('platform-ionic');

});

/**
 * Add new assignment
 */
app.controller('AddAssignmentCtrl', function ($scope, $http, $state, $ionicViewService, $ionicLoading, $ionicPopup, AddNewAssignment) {

	$scope.closeModalAddAssignment = function () {
		modalAssignment.hide();
	};

	/**
	 * Create new assignment based on course_id and class_id
	 */
	$scope.addAssignment = function (assignmentData) {

		if ($scope.linje.checked == true) {
			$scope.linje.checked = 1;
		} else {
			$scope.linje.checked = 0;
		}

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		AddNewAssignment.response(idToken, userId, assignmentData.name, assignmentData.description, class_id, course_id, $scope.linje.checked )
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				if (data.status == 'success') {
					modalAssignment.hide();
					$state.go($state.current, {}, {reload: true});
				}
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});
			});
	};
	$scope.linje = { checked: false };


});

/**
 * Edit assignment on current Class
 */
app.controller('EditAssignmentListCtrl', function ($scope, $state, $ionicLoading, $ionicNavBarDelegate, $ionicModal, $ionicPopup, $log, GetClassData, GetAssignmentData, UpdateAssignment, DeleteAssignment) {


	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Edit|delete assignment on selected class
	$ionicModal.fromTemplateUrl('templates/general/modal/edit_assignment.html', function (modal1) {
		modalEditAssignment = modal1;
	}, {
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true
	});

	// open Edit | Delete Assignment modal
	$scope.closeModalEditAssignment = function () {
		modalEditAssignment.hide();
	};

	// get Class data
	GetClassData.response(idToken, userId, class_id, course_id)
		.success(function (data) {
			//hide loader
			$ionicLoading.hide();
			$scope.students = data.students;
			$scope.assaignments = data.assaignments;
			$scope.lassaignments = data.lassaignments;
			$scope.marks = data.marks;

			console.group('Edit Assignment Section');
			$log.log(data.assaignments);
			console.groupEnd();
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// open Edit | Delete Assignment modal
	$scope.openModalEditAssignment = function (assignment_id) {
		// show modal
		modalEditAssignment.show();

		// get assignment data
		GetAssignmentData.response(idToken, userId, assignment_id)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.assignid = data.ID;
				$scope.name = data.name;
				$scope.description = data.description;
				$scope.linje = data.linje;
				if (data.linje == 0) {
					$scope.linje = { checked: false };
				} else {
					$scope.linje = { checked: true };
				}
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$scope.editAssignment = function (name, desc, linje, assignid) {
		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		if( linje == true ) {
			linje = 1;
		} else {
			linje = 0;
		}

		UpdateAssignment.response(idToken, userId, class_id, course_id, assignid, name, desc, linje)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				// hide modal
				modalEditAssignment.hide();
				GetClassData.response(idToken, userId, class_id, course_id)
					.success(function (data) {
						//hide loader
						$scope.students = data.students;
						$scope.assaignments = data.assaignments;
						$scope.lassaignments = data.lassaignments;
						$scope.marks = data.marks;
					})
					.error(function (error) {
						//hide loader
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});

	};

	$scope.deleteAssignment = function (assignid) {
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete Assignment',
			template: 'Are you sure you want to delete this asignment ?'
		});
		confirmPopup.then(function(res) {
			if(res) {
				// show loader
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});
				DeleteAssignment.response(idToken, userId, assignid)
					.success(function (data) {
						//hide loader
						$ionicLoading.hide();
						// hide modal
						modalEditAssignment.hide();

						GetClassData.response(idToken, userId, class_id, course_id)
							.success(function (data) {
								//hide loader
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
							})
							.error(function (error) {
								//hide loader
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: 'Something went worng!',
									template: error
								});
							});
						$ionicLoading.show({template: data.message, duration: 1500});
					})
					.error(function () {
						//hide loader
						$ionicLoading.hide();
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
			} else {
				$log.log('You are not sure');
			}
		});

	};
});

/**
 * Confirm Marks
 */
app.controller('ConfirmMarksCtrl', function ($scope, $stateParams, $http, $state, $log, $ionicViewService, $ionicLoading, $ionicPopup, GetConfirmMarksData, GetCourseDataByClassId) {
	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	GetConfirmMarksData.response(idToken, userId)
		.success(function (data) {
			//hide loader
			$ionicLoading.hide();

			$scope.classes = data.classes;
			$scope.classDefault = data.classes[0];

			class_id_vcd = data.classes[0].clsID;
			course_id_vcd = data.classes[0].coursesID;

			console.group('Get VCD Confirm Marks Class data on select');
			$log.log(data);
			console.groupEnd();
			/**
			 * Get all course by default selected class
			 *
			 */
			GetCourseDataByClassId.response(idToken, userId, class_id_vcd)
				.success(function (data) {
					if (data) {

						$scope.courses = data.courses;

						console.group('Get VCD Confirm Marks Course data List');
						$log.log(data);
						console.groupEnd();
					} else{
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: '!',
							template: error
						});
					}
					//hide loader
					$ionicLoading.hide();
				})
				.error(function (error) {
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: 'Something went worng!',
						template: error
					});
				});

			console.group('Get Marks Confirm Data Section');
			$log.log(data);
			$log.log($scope.classes);
			console.groupEnd();
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});



	/**
	 * Get class data on selecting class
	 *
	 * @param data
	 */
	$scope.getCourseData = function(data) {
		$log.log(data);
		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		class_id_vcd = data.clsID;
		course_id_vcd = data.coursesID;
		GetCourseDataByClassId.response(idToken, userId, class_id_vcd)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.courses = data.courses;

				console.group('Get Course Data List');
				$log.log(data);
				console.groupEnd();
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

});

/**
 * Confirm Marks
 */
app.controller('ConfirmMarksListCtrl', function ($scope, $stateParams, $http, $state, $ionicViewService, $ionicLoading, $ionicPopover, $ionicPopup, $log, $ionicNavBarDelegate, GetVcdData, ConfirmVcdMark, UnConfirmVcdMark, SendVcdClassMarks, ConfirmVcdAssignment, UnConfirmVcdAssignment) {
	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});
	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});
	GetVcdData.response(idToken, userId, $stateParams.class_id, $stateParams.course_id)
		.success(function (data) {
			if (data) {
				$scope.students = data.students;
				$scope.assaignments = data.assaignments;
				$scope.lassaignments = data.lassaignments;
				$scope.marks = data.marks;

				class_id_vcd = data.classID;
				course_id_vcd = data.courseID;

				// $log data
				console.group('Marks List for current Course/Class');
				$log.log(class_id_vcd);
				$log.log(course_id_vcd);
				$log.log(data);
				console.groupEnd();
			} else{
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
			}
			//hide loader
			$ionicLoading.hide();
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});
	$scope.sendMarks = function (data) {
		$log.log(JSON.stringify(data));
	};

	$scope.updateVcdMarks = function (assignmentId, studentId, mark) {

		console.group('Data marks send by VCD');
		$log.log('Assignment Id:'+ assignmentId);
		$log.log('Student id:'+ studentId);
		$log.log('Mark:'+ mark);
		console.groupEnd();

		if(!mark) {
			// alert notification where something went wrong
			$ionicLoading.show({template: 'Mark cannot be empty', duration: 1500});

		} else {
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});

			SendVcdClassMarks.response(idToken, userId, assignmentId, studentId, mark)
				.success(function (res) {
					//hide loader
					$ionicLoading.hide();
					$log.log('Result on sending Marks'+res);
					GetVcdData.response(idToken, userId, $stateParams.class_id, $stateParams.course_id)
						.success(function (data) {
							if (data) {
								$ionicLoading.show({template: data.message, duration: 1500});
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
								// $log data
								console.group('Marks List for current Course/Class');
								$log.log(data);
								console.groupEnd();
							} else{
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: '!',
									template: error
								});
							}
						})
						.error(function (error) {
							//hide loader
							$ionicLoading.hide();
							// alert notification where something went wrong
							var alertPopup = $ionicPopup.alert({
								title: 'Something went worng!',
								template: error
							});
						});
					$ionicLoading.show({template: res.message, duration: 1500});
				})
				.error(function (error) {
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: 'Something went worng!',
						template: error
					});
				});
		}

	};

	$scope.confirmVcdMark = function (studentId, assignmentId) {

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		// $log data
		console.group('Confirm VCD Mark');
		$log.log('StudentId:' + studentId);
		$log.log('AssignmentId:' + assignmentId);
		console.groupEnd();

		ConfirmVcdMark.response(idToken, userId, studentId, assignmentId)
			.success(function (data) {
				if (data) {
					$ionicLoading.hide();
					$ionicLoading.show({template: data.message, duration: 500});

					GetVcdData.response(idToken, userId, $stateParams.class_id, $stateParams.course_id)
						.success(function (data) {
							if (data) {
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
								// $log data
								console.group('Marks List for current Course/Class');
								$log.log(data);
								console.groupEnd();
							} else{
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: '!',
									template: error
								});
							}
						})
						.error(function (error) {
							//hide loader
							$ionicLoading.hide();
							// alert notification where something went wrong
							var alertPopup = $ionicPopup.alert({
								title: 'Something went worng!',
								template: error
							});
						});

					// $log data
					console.group('Response Confirm Mark');
					$log.log(data);
					console.groupEnd();
				} else{
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}

			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$scope.unConfirmVcdMark = function (studentId, assignmentId) {

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		// $log data
		console.group('UnConfirm VCD Mark');
		$log.log('StudentId:' + studentId);
		$log.log('AssignmentId:' + assignmentId);
		console.groupEnd();

		UnConfirmVcdMark.response(idToken, userId, studentId, assignmentId)
			.success(function (data) {
				if (data) {
					$ionicLoading.hide();
					$ionicLoading.show({template: data.message, duration: 500});

					GetVcdData.response(idToken, userId, $stateParams.class_id, $stateParams.course_id)
						.success(function (data) {
							if (data) {
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
								// $log data
								console.group('Marks List for current Course/Class');
								$log.log(data);
								console.groupEnd();
							} else{
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: '!',
									template: error
								});
							}
						})
						.error(function (error) {
							//hide loader
							$ionicLoading.hide();
							// alert notification where something went wrong
							var alertPopup = $ionicPopup.alert({
								title: 'Something went worng!',
								template: error
							});
						});

					// $log data
					console.group('Response UnConfirm Mark');
					$log.log(data);
					console.groupEnd();
				} else{
					//hide loader
					$ionicLoading.hide();

					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}

			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	}

	$scope.confirmVcdAssignment = function (assignmentId) {

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		// $log data
		console.group('Confirm VCD assignment');
		$log.log('AssignmentId:' + assignmentId);
		console.groupEnd();

		ConfirmVcdAssignment.response(idToken, userId, assignmentId)
			.success(function (data) {
				if (data) {
					$ionicLoading.hide();
					$ionicLoading.show({template: data.message, duration: 500});

					GetVcdData.response(idToken, userId, $stateParams.class_id, $stateParams.course_id)
						.success(function (data) {
							if (data) {
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
								// $log data
								console.group('Marks List for current Course/Class');
								$log.log(data);
								console.groupEnd();
							} else{
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: '!',
									template: error
								});
							}
						})
						.error(function (error) {
							//hide loader
							$ionicLoading.hide();
							// alert notification where something went wrong
							var alertPopup = $ionicPopup.alert({
								title: 'Something went worng!',
								template: error
							});
						});

					// $log data
					console.group('Response Confirm Mark');
					$log.log(data);
					console.groupEnd();
				} else{
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}

			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$scope.unConfirmVcdAssignment = function (assignmentId) {

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		// $log data
		console.group('Confirm VCD assignment');
		$log.log('AssignmentId:' + assignmentId);
		console.groupEnd();

		UnConfirmVcdAssignment.response(idToken, userId, assignmentId)
			.success(function (data) {
				if (data) {
					$ionicLoading.hide();
					$ionicLoading.show({template: data.message, duration: 500});

					GetVcdData.response(idToken, userId, $stateParams.class_id, $stateParams.course_id)
						.success(function (data) {
							if (data) {
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
								// $log data
								console.group('Marks List for current Course/Class');
								$log.log(data);
								console.groupEnd();
							} else{
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: '!',
									template: error
								});
							}
						})
						.error(function (error) {
							//hide loader
							$ionicLoading.hide();
							// alert notification where something went wrong
							var alertPopup = $ionicPopup.alert({
								title: 'Something went worng!',
								template: error
							});
						});

					// $log data
					console.group('Response Confirm Mark');
					$log.log(data);
					console.groupEnd();
				} else{
					//hide loader
					$ionicLoading.hide();
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}

			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$ionicPopover.fromTemplateUrl('templates/general/popover/marks_vcd_popover.html', {
		scope: $scope
	}).then(function(popover) {
		$scope.popover = popover;
	});

	document.body.classList.remove('platform-ios');
	document.body.classList.remove('platform-android');
	document.body.classList.add('platform-ionic');
});

/**
 * Add new vcd assignment
 */
app.controller('AddVcdAssignmentCtrl', function ($scope, $http, $state, $ionicViewService, $ionicLoading, $ionicPopup, $log, $ionicNavBarDelegate, AddNewVcdAssignment) {

	$scope.closeModalVcdAddAssignment = function () {
		modalVcdAssignment.hide();
	};

	/**
	 * Create new assignment based on course_id and class_id
	 */
	$scope.addVcdAssignment = function (VcdAssignmentData) {
		$log.log(VcdAssignmentData);
		if ($scope.linje.checked == true) {
			$scope.linje.checked = 1;
		} else {
			$scope.linje.checked = 0;
		}
		$log.log('ClassId:'+class_id_vcd);
		$log.log('CourseId:'+course_id_vcd);

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		AddNewVcdAssignment.response(idToken, userId, VcdAssignmentData.name, VcdAssignmentData.description, class_id_vcd, course_id_vcd)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				if (data.status == 'success') {
					modalVcdAssignment.hide();
					$state.go($state.current, {}, {reload: true});
				}
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});
			});
	};
	$scope.linje = { checked: false };


});

/**
 * Edit Vcd assignment on current Class
 */
app.controller('EditVcdAssignmentListCtrl', function ($scope, $state, $ionicLoading, $ionicModal,$ionicNavBarDelegate, $ionicPopup, $stateParams,$log, GetVcdData, GetAssignmentDataVcdToEdit, UpdateVcdAssignment, ConfirmAssignmentDeletion, UnConfirmAssignmentDeletion, DeleteAssignment) {

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Edit|delete assignment on selected class
	$ionicModal.fromTemplateUrl('templates/general/modal/edit_vcd_assignment.html', function (modal1) {
		modalEditVcdAssignment = modal1;
	}, {
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true
	});

	// open Edit | Delete Assignment modal
	$scope.closeModalEditVcdAssignment = function () {
		modalEditVcdAssignment.hide();
	};

	GetVcdData.response(idToken, userId, class_id_vcd, course_id_vcd)
		.success(function (data) {
			if (data) {
				$scope.students = data.students;
				$scope.assaignments = data.assaignments;
				$scope.lassaignments = data.lassaignments;
				$scope.marks = data.marks;

				class_id_vcd = data.classID;
				course_id_vcd = data.courseID;

				// $log data
				console.group('Marks List for current Course/Class');
				$log.log(class_id_vcd);
				$log.log(course_id_vcd);
				$log.log(data);
				console.groupEnd();
			} else{
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
			}
			//hide loader
			$ionicLoading.hide();
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// open Edit | Delete Assignment modal
	$scope.openModalEditVcdAssignment = function (assignment_id) {
		// show modal
		modalEditVcdAssignment.show();

		// get assignment data
		GetAssignmentDataVcdToEdit.response(idToken, userId, assignment_id)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.assignid = data.ID;
				$scope.name = data.name;
				$scope.description = data.description;
				$scope.linje = data.linje;
				if (data.linje == 0) {
					$scope.linje = { checked: false };
				} else {
					$scope.linje = { checked: true };
				}
			})
			.error(function (error) {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$scope.editVcdAssignment = function (name, desc, linje, assignid) {
		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		// $log data
		console.group('Update Vcd Assignment Data:');
		$log.log(assignid);
		$log.log(name);
		$log.log(desc);
		console.groupEnd();

		UpdateVcdAssignment.response(idToken, userId, assignid, name, desc)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				// hide modal
				modalEditVcdAssignment.hide();

				GetVcdData.response(idToken, userId, class_id_vcd, course_id_vcd)
					.success(function (data) {
						if (data) {
							$scope.students = data.students;
							$scope.assaignments = data.assaignments;
							$scope.lassaignments = data.lassaignments;
							$scope.marks = data.marks;

							class_id_vcd = data.classID;
							course_id_vcd = data.courseID;

							// $log data
							console.group('Marks List for current Course/Class');
							$log.log(class_id_vcd);
							$log.log(course_id_vcd);
							$log.log(data);
							console.groupEnd();
						} else{
							//hide loader
							$ionicLoading.hide();
							var alertPopup = $ionicPopup.alert({
								title: '!',
								template: error
							});
						}

					})
					.error(function (error) {
						//hide loader
						$ionicLoading.hide();
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});

	};

	$scope.confirmAssignmentDeletion = function (assignment_id) {
		ConfirmAssignmentDeletion.response(idToken, userId, assignment_id)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				// hide modal
				GetVcdData.response(idToken, userId, class_id_vcd, course_id_vcd)
					.success(function (data) {
						if (data) {
							$scope.students = data.students;
							$scope.assaignments = data.assaignments;
							$scope.lassaignments = data.lassaignments;
							$scope.marks = data.marks;

							class_id_vcd = data.classID;
							course_id_vcd = data.courseID;

							// $log data
							console.group('Marks List for current Course/Class');
							$log.log(class_id_vcd);
							$log.log(course_id_vcd);
							$log.log(data);
							console.groupEnd();
						} else{
							//hide loader
							$ionicLoading.hide();
							var alertPopup = $ionicPopup.alert({
								title: '!',
								template: error
							});
						}

					})
					.error(function (error) {
						//hide loader
						$ionicLoading.hide();
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$scope.unConfirmAssignmentDeletion = function (assignment_id) {
		UnConfirmAssignmentDeletion.response(idToken, userId, assignment_id)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				// hide modal
				GetVcdData.response(idToken, userId, class_id_vcd, course_id_vcd)
					.success(function (data) {
						if (data) {
							$scope.students = data.students;
							$scope.assaignments = data.assaignments;
							$scope.lassaignments = data.lassaignments;
							$scope.marks = data.marks;

							class_id_vcd = data.classID;
							course_id_vcd = data.courseID;

							// $log data
							console.group('Marks List for current Course/Class');
							$log.log(class_id_vcd);
							$log.log(course_id_vcd);
							$log.log(data);
							console.groupEnd();
						} else{
							//hide loader
							$ionicLoading.hide();
							var alertPopup = $ionicPopup.alert({
								title: '!',
								template: error
							});
						}

					})
					.error(function (error) {
						//hide loader
						$ionicLoading.hide();
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

	$scope.deleteAssignment = function (assignid) {
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete Assignment',
			template: 'Are you sure you want to delete this asignment ?'
		});
		confirmPopup.then(function(res) {
			if(res) {
				// show loader
				$ionicLoading.show({
					content: 'Loading',
					animation: 'fade-in',
					showBackdrop: true,
					maxWidth: 200,
					showDelay: 0
				});
				DeleteAssignment.response(idToken, userId, assignid)
					.success(function (data) {
						//hide loader
						$ionicLoading.hide();
						// hide modal
						modalEditAssignment.hide();

						GetClassData.response(idToken, userId, class_id, course_id)
							.success(function (data) {
								//hide loader
								$scope.students = data.students;
								$scope.assaignments = data.assaignments;
								$scope.lassaignments = data.lassaignments;
								$scope.marks = data.marks;
							})
							.error(function (error) {
								//hide loader
								// alert notification where something went wrong
								var alertPopup = $ionicPopup.alert({
									title: 'Something went worng!',
									template: error
								});
							});
						$ionicLoading.show({template: data.message, duration: 1500});
					})
					.error(function () {
						//hide loader
						$ionicLoading.hide();
						// alert notification where something went wrong
						var alertPopup = $ionicPopup.alert({
							title: 'Something went worng!',
							template: error
						});
					});
			} else {
				$log.log('You are not sure');
			}
		});

	};
});
