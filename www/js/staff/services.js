
/**
 * Staff
 */
app.factory('StaffData', function ($http, Config) {
	var data = {};
	data.getStaff = function ($tokId, $userId, $page, $name) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Staff/staffjson?sid='+$tokId+'&userId='+$userId+'&page='+$page+'&name='+$name
			}
		);
	};
	return data;
});

app.factory('StaffActive', function ($http, Config) {
	var data = {};
	data.active = function ($tokId, $userId, $staff_id, $state) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Staff/active?sid='+$tokId+'&userId='+$userId+'&staff_id='+$staff_id+'&state='+$state
			}
		);
	};
	return data;
});

app.factory('ChangeStaffPassword', function ($http, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $saff_id, $new_password) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Staff/update_password?sid='+$tokId+'&userId='+$userId+'&staffId='+$saff_id+'&newPassword='+$new_password
			}
		);
	};
	return data;
});

app.factory('ViewProfileData', function ($http, Config) {
	var data = {};
	data.profileData = function ($tokId, $userId, $profile_id, $profile_type) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'General/getViewProfileData?sid='+$tokId+'&userId='+$userId+'&profile_id='+$profile_id+'&type='+$profile_type
			}
		);
	};
	return data;
});

app.factory('GetMarksClass', function ($http, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Marks/?sid='+$tokId+'&userId='+$userId
			}
		);
	};
	return data;
});

app.factory('GetClassData', function ($http, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $class_id, $course_id) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Marks/get_class_data?sid='+$tokId+'&userId='+$userId+'&classID='+$class_id+'&courseID='+$course_id
			}
		);
	};
	return data;
});

app.factory('SendClassMarks', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $marks) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/send',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				marks: $marks
			})
		});
	};
	return data;
});

app.factory('AddNewAssignment', function ($http, Config) {
	console.log('Send factuary');
	var data = {};
	data.response = function ($tokId, $userId, $name, $description, $class_id, $course_id, $linje) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Marks/add_assignement?sid='+$tokId+'&userId='+$userId+'&classID='+$class_id+'&courseID='+$course_id+'&linja='+$linje+'&name='+$name+'&description='+$description+'&weight='+''
			}
		);
	};
	return data;
});

app.factory('GetAssignmentData', function ($http, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignment_id) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Marks/get_assignment?sid='+$tokId+'&userId='+$userId+'&assignment_id='+$assignment_id
			}
		);
	};
	return data;
});

app.factory('UpdateAssignment', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $class_id, $course_id, $assignId, $name, $desc, $linje) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/update_assignment',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				class_id: $class_id,
				course_id: $course_id,
				assaignementID: $assignId,
				name: $name,
				description: $desc,
				linja: $linje
			})
		});
	};
	return data;
});

app.factory('DeleteAssignment', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/delete_assignment',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignment_id: $assignId
			})
		});
	};
	return data;
});

app.factory('GetConfirmMarksData', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/confirm',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('GetCourseDataByClassId', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $classId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/getCoursesByClassId',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				clsID: $classId
			})
		});
	};
	return data;
});

app.factory('GetVcdData', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $classId, $courseId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/get_vcd_data',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				classID: $classId,
				courseID: $courseId
			})
		});
	};
	return data;
});

app.factory('SendVcdClassMarks', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignmentId, $studentId, $mark) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/submit_vcd',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignmentId: $assignmentId,
				studentId: $studentId,
				vcdMark: $mark
			})
		});
	};
	return data;
});

app.factory('ConfirmVcdMark', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $studentId, $assignmentId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/confirm_mark_vcd',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignement: $assignmentId,
				studentid: $studentId
			})
		});
	};
	return data;
});

app.factory('UnConfirmVcdMark', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $studentId, $assignmentId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/unconfirm_mark_vcd',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignement: $assignmentId,
				studentid: $studentId
			})
		});
	};
	return data;
});

app.factory('AddNewVcdAssignment', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $name, $description, $class_id, $course_id) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/add_vcd_assignement',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				name: $name,
				description: $description,
				classID: $class_id,
				courseID: $course_id
			})
		});
	};
	return data;
});

app.factory('GetAssignmentDataVcdToEdit', function ($http, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignment_id) {
		return $http(
			{
				method: 'GET', url:Config.ApiTokUrl+'Marks/get_assignment?sid='+$tokId+'&userId='+$userId+'&assignment_id='+$assignment_id
			}
		);
	};
	return data;
});

app.factory('UpdateVcdAssignment', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignId, $name, $desc) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/update_vcd_assignment',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assaignementID: $assignId,
				name: $name,
				description: $desc
			})
		});
	};
	return data;
});

app.factory('ConfirmVcdAssignment', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/confirm_assignment_vcd',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignmentid: $assignId
			})
		});
	};
	return data;
});

app.factory('UnConfirmVcdAssignment', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/unconfirm_assignment_vcd',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignmentid: $assignId
			})
		});
	};
	return data;
});

app.factory('ConfirmAssignmentDeletion', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/confirm_assignment_deletion',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignmentID: $assignId
			})
		});
	};
	return data;
});

app.factory('UnConfirmAssignmentDeletion', function ($http,$httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $assignId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Marks/deny_assignment_deletion',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				assignmentID: $assignId
			})
		});
	};
	return data;
});