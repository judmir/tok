/**
 * Quiz
 */
app.factory('QuizService', function ($http, Config) {
    var data = {};
    data.getQuizzes = function ($tokId, $userId, $page, $name) {
        return $http(
            {
                method: 'GET',
                url: Config.ApiTokUrl + 'quizes/getall?sid=' + $tokId + '&userId=' + $userId + '&page=' + $page
            }
        );
    };
    return data;
});

/**
 * Get participation based on quiz id
 */
app.factory('ParticipationService', function ($http, Config) {
    var data = {};
    data.getParticipation = function ($tokId, $userId, $participationId) {
        return $http(
            {
                method: 'GET',
                url: Config.ApiTokUrl + 'quizes/getParticipation?sid=' + $tokId + '&userId=' + $userId + '&participationId=' + $participationId
            }
        );
    };
    return data;
});
