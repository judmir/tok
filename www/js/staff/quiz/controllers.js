app.controller('QuizCtrl', function ($scope, $ionicLoading, $log, $state, $ionicNavBarDelegate, QuizService) {
    // set data
    $scope.items = [];
    $scope.times = 1;
    $scope.postsCompleted = false;
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true;

    $scope.$on('$ionicView.enter', function(e) {
        $ionicNavBarDelegate.showBar(true);
    });

    /**
     * Get all staff
     */
    $scope.getQuizzes = function () {

        QuizService.getQuizzes(idToken, userId, $scope.times)
            .success(function (data) {

                $scope.items = $scope.items.concat(data.results);

                $scope.path = data.urlPath;
                $scope.$broadcast('scroll.infiniteScrollComplete');

                $scope.times = $scope.times + 1;
                if (data.more == 'false' || data.more == false || !data.more) {
                    $scope.postsCompleted = true;
                }

                console.group('Show all Quizzes');
                $log.log(data);
                console.groupEnd();
            })
            .error(function (error) {
                $scope.items = [];
            });
    };

    // pull to refresh buttons
    $scope.doRefresh = function () {
        $scope.times = 1;
        $scope.items = [];
        $scope.postsCompleted = false;
        $state.go($state.current, {}, {reload: true});
        $scope.$broadcast('scroll.refreshComplete');
    };

});

app.controller('ParticipationCtrl', function ($scope, $ionicLoading, $log, $state, $ionicNavBarDelegate, $stateParams, ParticipationService ) {

    console.group('Participation Controller');

    // participation id
    $log.log($stateParams.participationId, 'Participation ID');
    $participationId = $stateParams.participationId;

    ParticipationService.getParticipation(idToken, userId, $participationId)
        .success(function (data) {
            $scope.items = data.results;
            $log.log(data, 'ParticipationData');
        })
        .error(function (error) {
            $scope.items = [];
        });
    console.groupEnd();

});


