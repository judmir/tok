app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app.quiz', {
            url: "/quizes",
            cache: true,
            views: {
                'menuContent': {
                    templateUrl: "staff/quiz/list.html",
                    controller: "QuizCtrl"
                }
            }
        })
        .state('app.participation', {
            url: '/quizes/participation/:participationId',
            views: {
                'menuContent' : {
                    templateUrl: "staff/quiz/participation.html",
                    controller: "ParticipationCtrl"
                }
            }
        });

    $urlRouterProvider.otherwise('/app/dashboard');
});