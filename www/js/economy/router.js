app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('app.economy',{
			url: "/economy",
			views: {
				'menuContent': {
					templateUrl: "staff/economy/economy.html",
					controller: 'EconomyCtrl'
				}
			}
		})
		.state('app.income',{
			url: "/income",
			views: {
				'menuContent': {
					templateUrl: "staff/economy/income.html",
					controller: 'IncomeCtrl'
				}
			}
		})
		.state('app.outcome',{
			url: "/outcome",
			views: {
				'menuContent': {
					templateUrl: "staff/economy/outcome.html",
					controller: 'OutcomeCtrl'
				}
			}
		})
		.state('app.admin',{
			url: "/economy/admin",
			views: {
				'menuContent': {
					templateUrl: "staff/economy/admin/economy.html",
					controller: 'EconomyAdminCtrl'
				}
			}
		})

		.state('app.incomeAdmin',{
			url: "/income/admin",
			views: {
				'menuContent': {
					templateUrl: "staff/economy/admin/income.html",
					controller: 'IncomeAdminCtrl'
				}
			}
		})

		.state('app.outcomeAdmin',{
			url: "/outcome/admin",
			views: {
				'menuContent': {
					templateUrl: "staff/economy/admin/outcome.html",
					controller: 'OutcomeAdminCtrl'
				}
			}
		});
		$urlRouterProvider.otherwise('/app/dashboard');
});