
/**
 * Economy Data
 */
app.factory('GetEconomyData', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

/** ========================  Income Module   ===============================*/
app.factory('GetIncomeList', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_income_list',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('GetCategoryTa', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_category_ta',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('AddIncome', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $incomeText, $incomeAmount, $$incomeCategory, $incomeProcessed) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/submit_income',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				income_text: $incomeText,
				income_amount: $incomeAmount,
				income_category: $$incomeCategory,
				income_processed: $incomeProcessed
			})
		});
	};
	return data;
});

app.factory('ConfirmIncome', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $incomeId, $flag) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/confirm_income',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				incomeId: $incomeId,
				flag: $flag
			})
		});
	};
	return data;
});

app.factory('DeleteIncome', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $incomeId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/delete_income',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				incomeId: $incomeId
			})
		});
	};
	return data;
});

/** ========================  Outcome Module   ===============================*/

app.factory('GetOutcomeList', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_outcome_list',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('GetCategorySh', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_category_sh',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('ConfirmOutcome', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $incomeId, $flag) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/confirm_expense',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				incomeId: $incomeId,
				flag: $flag
			})
		});
	};
	return data;
});

app.factory('AddOutcome', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $outcomeText, $outcomeAmount, $$outcomeCategory, $outcomeProcessed) {
		console.log($outcomeText,$outcomeText, $outcomeAmount, $$outcomeCategory, $outcomeProcessed);
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/submit_expense',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				outcome_text: $outcomeText,
				outcome_amount: $outcomeAmount,
				outcome_category: $$outcomeCategory,
				outcome_processed: $outcomeProcessed
			})
		});
	};
	return data;
});

app.factory('DeleteOutcome', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $outcomeId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/delete_expense',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				outcomeId: $outcomeId
			})
		});
	};
	return data;
});

/**
 * Economy ADMIN Data
 */

app.factory('GetEconomyAdminData', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

/** ========================  Income Module   ===============================*/
app.factory('GetIncomeAdminList', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_income_list_admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('GetEconomistsList', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_economists_list',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId
			})
		});
	};
	return data;
});

app.factory('AddIncomeAdmin', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $incomeText, $incomeAmount, $$incomeCategory, $$incomeEconomist, $incomeProcessed) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/submit_income_admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				income_text: $incomeText,
				income_amount: $incomeAmount,
				income_category: $$incomeCategory,
				income_processed: $incomeProcessed,
				income_economist: $$incomeEconomist
			})
		});
	};
	return data;
});

app.factory('ConfirmIncomeAdmin', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $incomeId, $flag) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/confirm_income_admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				incomeId: $incomeId,
				flag: $flag
			})
		});
	};
	return data;
});

app.factory('GetOutcomeAdminList', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/get_outcome_list_admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId
			})
		});
	};
	return data;
});

app.factory('ConfirmOutcomeAdmin', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $id, $flag) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/confirm_expense_admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				outcomeId: $id,
				flag: $flag
			})
		});
	};
	return data;
});

app.factory('AddOutcomeAdmin', function ($http, $httpParamSerializer, Config) {
	var data = {};
	data.response = function ($tokId, $userId, $outcomeText, $outcomeAmount, $$outcomeCategory, $outcomeProcessed, $outcomeEconomist) {
		return  $http({
			method: "POST",
			url:Config.ApiTokUrl+'Economy/submit_expense_admin',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			data: $httpParamSerializer({
				sid: $tokId,
				userId: $userId,
				outcome_text: $outcomeText,
				outcome_amount: $outcomeAmount,
				outcome_category: $$outcomeCategory,
				outcome_processed: $outcomeProcessed,
				outcome_economist: $outcomeEconomist
			})
		});
	};
	return data;
});