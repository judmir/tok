/***************************************************************************************************************************
 * Economy controller, get economy statistics about , Balance, Income and Outcome values
 ***************************************************************************************************************************/
app.controller('EconomyCtrl', function ($scope, $ionicLoading, $log, $state, $ionicNavBarDelegate, $ionicPopup, GetEconomyData) {
	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Get economy data , Balance , Income, Outcome
	GetEconomyData.response(idToken, userId)
		.success(function (data) {
			if (data) {

				// $log data
				console.group('Economy data response , Balance, Income, outcome values');
				$log.log(data);
				$log.log(data.SK_te_ardhura);
				$log.log(data);
				console.groupEnd();

				$scope.balance = data.SK_te_ardhura - data.SK_shpenzime;
				$scope.balance_debt = data.S_te_ardhura - data.S_shpenzime;

				$scope.income = data.SK_te_ardhura;
				$scope.income_debt = data.S_te_ardhura;

				$scope.outcome = data.SK_shpenzime;
				$scope.outcome_debt = data.S_shpenzime;

				//hide loader
				$ionicLoading.hide();
			} else{
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
				//hide loader
				$ionicLoading.hide();
			}
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

});

app.controller('IncomeCtrl', function ($scope, $ionicLoading, $ionicPopup, $ionicModal, $log, $state, $ionicNavBarDelegate, GetCategoryTa, GetIncomeList, ConfirmIncome, AddIncome, DeleteIncome) {

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	// Edit|delete assignment on selected class
	$ionicModal.fromTemplateUrl('templates/general/modal/economy/add_income.html', function (modal1) {
		modalAddIncome = modal1;
	}, {
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true
	});

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Get income categories
	GetCategoryTa.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Categories Ta');
				console.log(data);
				console.groupEnd();
				$scope.categoriesTa = data.kategori_TA;
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// Get income categories
	GetIncomeList.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Income Data List');
				console.log(data);
				console.groupEnd();
				$scope.incomeList = data.income_list;
				//hide loader
				$ionicLoading.hide();
			} else {
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});


	// Open modal to add new income
	$scope.addIncomeModal = function () {
		modalAddIncome.show();
	};

	/**
	 * Confirm/Unconfirm Income based on flag
	 *
	 * @param incomeId
	 * @param flag
	 */
	$scope.confirmIncome = function (incomeId, flag) {

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		ConfirmIncome.response(idToken, userId, incomeId, flag)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.incomeList = data.income_list;
				$ionicLoading.show({template: data.message, duration: 1500});

			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
			})
	};

	/**
	 * Delete income
	 *
	 * @param $incomeId
	 */
	$scope.deleteIncome = function (index , incomeId) {

		console.log('test');
		// Confirm dialog on delete income
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete Income',
			template: 'Are you sure you want to delete this income?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				DeleteIncome.response(idToken, userId, incomeId)
					.success(function (data) {
						//hide loader
						$ionicLoading.hide();
						$ionicLoading.show({template: data.message, duration: 1500});
						$scope.incomeList.splice(index, 1);
					})
					.error(function () {
						//hide loader
						$ionicLoading.hide();
					})
			} else {
				console.log('You are not sure');
			}
		});
	};

	$scope.addIncome = function (IncomeData) {
		$log.log('IncomeData: ', IncomeData);

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		AddIncome.response(idToken, userId, IncomeData.income_text, IncomeData.income_amount, IncomeData.income_category, IncomeData.income_processed)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();

				if (data.status == 'success') {
					modalAddIncome.hide();
					//$state.go($state.current, {}, {reload: true});
					$scope.incomeList.unshift(data.last_income);
				}

				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {

				//hide loader
				$ionicLoading.hide();

				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});

			});
	};

	$scope.closeModalAddIncome = function () {
		modalAddIncome.hide();
	};

	// pull to refresh buttons
	$scope.doRefresh = function () {
		// Get income categories
		GetIncomeList.response(idToken, userId)
			.success(function (data) {
				if(data) {
					console.group('Income Data List');
					console.log(data);
					console.groupEnd();
					$scope.incomeList = data.income_list;
					//hide loader
					$ionicLoading.hide();
				} else {
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}
				$scope.$broadcast('scroll.refreshComplete');
			})
			.error(function () {
				$scope.$broadcast('scroll.refreshComplete');
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

});

app.controller('OutcomeCtrl', function ($scope, $ionicLoading, $ionicPopup, $ionicModal, $log, $state, $ionicNavBarDelegate, GetOutcomeList, GetCategorySh, ConfirmOutcome, AddOutcome, DeleteOutcome) {

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	$ionicModal.fromTemplateUrl('templates/general/modal/economy/add_outcome.html', function (modal1) {
		modalAddOutcome = modal1;
	}, {
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true
	});

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Get outcome categories
	GetCategorySh.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Categories Sh');
				console.log(data);
				console.groupEnd();
				$scope.categoriesSh = data.kategori_SH;
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// Get income categories
	GetOutcomeList.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Income Data List');
				console.log(data);
				console.groupEnd();
				$scope.outcomeList = data.outcome_list;
				//hide loader
				$ionicLoading.hide();
			} else {
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});


	// Open modal to add new income
	$scope.addOutcomeModal = function () {
		modalAddOutcome.show();
	};

	/**
	 * Confirm/Unconfirm outcome based on flag
	 *
	 * @param incomeId
	 * @param flag
	 */
	$scope.confirmOutcome = function (incomeId, flag) {

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		ConfirmOutcome.response(idToken, userId, incomeId, flag)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.outcomeList = data.outcome_list;
				$ionicLoading.show({template: data.message, duration: 1500});

			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
			})
	};

	/**
	 * Delete income
	 *
	 * @param $incomeId
	 */
	$scope.deleteOutcome = function (index , outcomeId) {

		console.log('test');
		// Confirm dialog on delete outcome
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete outcome',
			template: 'Are you sure you want to delete this Outcome?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				DeleteOutcome.response(idToken, userId, outcomeId)
					.success(function (data) {
						//hide loader
						$ionicLoading.hide();
						$ionicLoading.show({template: data.message, duration: 1500});
						$scope.outcomeList.splice(index, 1);
					})
					.error(function () {
						//hide loader
						$ionicLoading.hide();
					})
			} else {
				console.log('You are not sure');
			}
		});
	};

	$scope.addOutcome = function (OutcomeData) {
		$log.log('OutcomeData: ', OutcomeData);

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		AddOutcome.response(idToken, userId, OutcomeData.outcome_text, OutcomeData.outcome_amount, OutcomeData.outcome_category, OutcomeData.outcome_processed)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();

				if (data.status == 'success') {
					modalAddOutcome.hide();
					$scope.outcomeList.unshift(data.last_outcome);
					console.log(data.last_outcome, 'Data LastOutcome');
				}

				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {

				//hide loader
				$ionicLoading.hide();

				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});

			});
	};

	$scope.closeModalAddOutcome = function () {
		modalAddOutcome.hide();
	};

	// pull to refresh buttons
	$scope.doRefresh = function () {
		GetOutcomeList.response(idToken, userId)
			.success(function (data) {
				if(data) {
					console.group('Income Data List');
					console.log(data);
					console.groupEnd();
					$scope.outcomeList = data.outcome_list;
					//hide loader
					$ionicLoading.hide();
				} else {
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}
				$scope.$broadcast('scroll.refreshComplete');
			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
				$scope.$broadcast('scroll.refreshComplete');
			});
	};
});


/***************************************************************************************************************************
 * ADMIN Economy controller, get economy statistics about , Balance, Income and Outcome values
 ***************************************************************************************************************************/

app.controller('EconomyAdminCtrl', function ($scope, $ionicLoading, $log, $state, $ionicNavBarDelegate, $ionicPopup, GetEconomyAdminData) {
	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Get economy data , Balance , Income, Outcome
	GetEconomyAdminData.response(idToken, userId)
		.success(function (data) {
			if (data) {

				// $log data
				console.group('Economy data response , Balance, Income, outcome values');
				$log.log(data);
				$log.log(data.SK_te_ardhura);
				$log.log(data);
				console.groupEnd();

				$scope.balance = data.SK_te_ardhura - data.SK_shpenzime;
				$scope.balance_debt = data.S_te_ardhura - data.S_shpenzime;

				$scope.income = data.SK_te_ardhura;
				$scope.income_debt = data.S_te_ardhura;

				$scope.outcome = data.SK_shpenzime;
				$scope.outcome_debt = data.S_shpenzime;

				//hide loader
				$ionicLoading.hide();
			} else{
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
				//hide loader
				$ionicLoading.hide();
			}
		})
		.error(function (error) {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

});

app.controller('IncomeAdminCtrl', function ($scope, $ionicLoading, $ionicPopup, $ionicModal, $log, $state, $ionicNavBarDelegate, GetCategoryTa, GetEconomistsList, GetIncomeAdminList, ConfirmIncomeAdmin, AddIncomeAdmin, DeleteIncome) {

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	// Edit|delete assignment on selected class
	$ionicModal.fromTemplateUrl('templates/general/modal/economy/admin/add_income.html', function (modal1) {
		modalAddIncomeAdmin = modal1;
	}, {
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true
	});

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Get income categories
	GetCategoryTa.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Categories Ta');
				console.log(data);
				console.groupEnd();
				$scope.categoriesTa = data.kategori_TA;
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// Get economists list
	GetEconomistsList.response(idToken)
		.success(function (data) {
			if(data) {
				console.group('Economists List (STAFF)');
				console.log(data);
				console.groupEnd();
				$scope.staff = data.staff;
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// Get income categories
	GetIncomeAdminList.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Income Data List Admin');
				console.log(data);
				console.groupEnd();
				$scope.incomeList = data.income_list;
				//hide loader
				$ionicLoading.hide();
			} else {
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});


	// Open modal to add new income
	$scope.addIncomeAdminModal = function () {
		modalAddIncomeAdmin.show();
	};

	/**
	 * Confirm/Unconfirm Income based on flag
	 *
	 * @param incomeId
	 * @param flag
	 */
	$scope.confirmIncome = function (incomeId, flag) {

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		ConfirmIncomeAdmin.response(idToken, userId, incomeId, flag)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.incomeList = data.income_list;
				$ionicLoading.show({template: data.message, duration: 1500});

			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
			})
	};

	/**
	 * Delete income
	 *
	 * @param $incomeId
	 */
	$scope.deleteIncome = function (index , incomeId) {

		console.log('test');
		// Confirm dialog on delete income
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete Income',
			template: 'Are you sure you want to delete this income?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				DeleteIncome.response(idToken, userId, incomeId)
					.success(function (data) {
						//hide loader
						$ionicLoading.hide();
						$ionicLoading.show({template: data.message, duration: 1500});
						$scope.incomeList.splice(index, 1);
					})
					.error(function () {
						//hide loader
						$ionicLoading.hide();
					})
			} else {
				console.log('You are not sure');
			}
		});
	};

	$scope.addIncome = function (IncomeData) {
		$log.log('IncomeData: ', IncomeData);

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		AddIncomeAdmin.response(idToken, userId, IncomeData.income_text, IncomeData.income_amount, IncomeData.income_category, IncomeData.income_economist, IncomeData.income_processed)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();

				if (data.status == 'success') {
					modalAddIncomeAdmin.hide();
					//$state.go($state.current, {}, {reload: true});
					$scope.incomeList.unshift(data.last_income);
				}

				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {

				//hide loader
				$ionicLoading.hide();

				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});

			});
	};

	$scope.closeModalAddIncome = function () {
		modalAddIncomeAdmin.hide();
	};

	// pull to refresh buttons
	$scope.doRefresh = function () {
		// Get income categories
		GetIncomeAdminList.response(idToken, userId)
			.success(function (data) {
				if(data) {
					console.group('Income Data List Admin');
					console.log(data);
					console.groupEnd();
					$scope.incomeList = data.income_list;
					//hide loader
					$ionicLoading.hide();
				} else {
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}
				$scope.$broadcast('scroll.refreshComplete');
			})
			.error(function () {
				$scope.$broadcast('scroll.refreshComplete');
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});
	};

});

app.controller('OutcomeAdminCtrl', function ($scope, $ionicLoading, $ionicPopup, $ionicModal, $log, $state, $ionicNavBarDelegate, GetOutcomeAdminList, GetCategorySh, GetEconomistsList, ConfirmOutcomeAdmin, AddOutcomeAdmin, DeleteOutcome) {

	$scope.$on('$ionicView.enter', function(e) {
		$ionicNavBarDelegate.showBar(true);
	});

	$ionicModal.fromTemplateUrl('templates/general/modal/economy/admin/add_outcome.html', function (modal1) {
		modalAddOutcomeAdmin = modal1;
	}, {
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true
	});

	// show loader
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	// Get outcome categories
	GetCategorySh.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Categories Sh');
				console.log(data);
				console.groupEnd();
				$scope.categoriesSh = data.kategori_SH;
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	// Get Outcome list
	GetOutcomeAdminList.response(idToken, userId)
		.success(function (data) {
			if(data) {
				console.group('Outcome Admin Data List');
				console.log(data);
				console.groupEnd();
				$scope.outcomeList = data.outcome_list;
				//hide loader
				$ionicLoading.hide();
			} else {
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: '!',
					template: error
				});
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});


	// Open modal to add new income
	$scope.addOutcomeModal = function () {
		modalAddOutcomeAdmin.show();
	};

	// Get economists list
	GetEconomistsList.response(idToken)
		.success(function (data) {
			if(data) {
				console.group('Economists List (STAFF)');
				console.log(data);
				console.groupEnd();
				$scope.staff = data.staff;
			}
		})
		.error(function () {
			//hide loader
			$ionicLoading.hide();
			// alert notification where something went wrong
			var alertPopup = $ionicPopup.alert({
				title: 'Something went worng!',
				template: error
			});
		});

	/**
	 * Confirm/Unconfirm outcome based on flag
	 *
	 * @param incomeId
	 * @param flag
	 */
	$scope.confirmOutcome = function (outcomeId, flag) {

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		ConfirmOutcomeAdmin.response(idToken, userId, outcomeId, flag)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();
				$scope.outcomeList = data.outcome_list;
				$ionicLoading.show({template: data.message, duration: 1500});

			})
			.error(function () {
				//hide loader
				$ionicLoading.hide();
			})
	};

	/**
	 * Delete income
	 *
	 * @param $incomeId
	 */
	$scope.deleteOutcome = function (index , outcomeId) {

		console.log('test');
		// Confirm dialog on delete outcome
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete outcome',
			template: 'Are you sure you want to delete this Outcome?'
		});

		confirmPopup.then(function(res) {
			if(res) {
				DeleteOutcome.response(idToken, userId, outcomeId)
					.success(function (data) {
						//hide loader
						$ionicLoading.hide();
						$ionicLoading.show({template: data.message, duration: 1500});
						$scope.outcomeList.splice(index, 1);
					})
					.error(function () {
						//hide loader
						$ionicLoading.hide();
					})
			} else {
				console.log('You are not sure');
			}
		});
	};

	$scope.addOutcome = function (OutcomeData) {
		$log.log('OutcomeData: ', OutcomeData);

		// show loader
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		AddOutcomeAdmin.response(idToken, userId, OutcomeData.outcome_text, OutcomeData.outcome_amount, OutcomeData.outcome_category, OutcomeData.outcome_processed, OutcomeData.outcome_economist)
			.success(function (data) {
				//hide loader
				$ionicLoading.hide();

				if (data.status == 'success') {
					modalAddOutcomeAdmin.hide();
					$scope.outcomeList.unshift(data.last_outcome);
					console.log(data.last_outcome, 'Data LastOutcome');
				}

				$ionicLoading.show({template: data.message, duration: 1500});
			})
			.error(function (error) {

				//hide loader
				$ionicLoading.hide();

				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went wrong!',
					template: error
				});

			});
	};

	$scope.closeModalAddOutcome = function () {
		modalAddOutcomeAdmin.hide();
	};

	// pull to refresh buttons
	$scope.doRefresh = function () {
		// Get Outcome list
		GetOutcomeAdminList.response(idToken, userId)
			.success(function (data) {
				if(data) {
					console.group('Outcome Admin Data List');
					console.log(data);
					console.groupEnd();
					$scope.outcomeList = data.outcome_list;
					//hide loader
					$ionicLoading.hide();
				} else {
					// alert notification where something went wrong
					var alertPopup = $ionicPopup.alert({
						title: '!',
						template: error
					});
				}
				$scope.$broadcast('scroll.refreshComplete');
			})
			.error(function () {
				$scope.$broadcast('scroll.refreshComplete');
				//hide loader
				$ionicLoading.hide();
				// alert notification where something went wrong
				var alertPopup = $ionicPopup.alert({
					title: 'Something went worng!',
					template: error
				});
			});

	};

});
