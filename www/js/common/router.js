app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            cache: false,
            templateUrl: "general/sidebar-menu.html",
            controller: "MainCtrl"
        })

        .state('intro', {
            url: "/intro",
            templateUrl: "general/intro.html",
            controller: "IntroCtrl"
        })

        .state('login', {
            url: "/login",
            templateUrl: "general/login.html",
            controller: "LoginCtrl"
        })

        .state('app.profile', {
            url: "/profile",
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: "general/profile.html",
                    controller: "ProfileCtrl"
                }
            }
        })

        .state('app.dashboard', {
            url: "/dashboard",
            views: {
                'menuContent': {
                    templateUrl: "general/dashboard.html",
                    controller: "DashboardCtrl"
                }
            }
        });

    //  login page
    $urlRouterProvider.otherwise('/app/dashboard');
});