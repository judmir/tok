app.constant("Config", {
  "ApiTokUrl" : "http://beta.api.turgutozalcollege.com/",
  "StuffProfileImgUrl" : "data/stuff_portal/app_files/user_profile_pictures/",
  "StudentProfileImgUrl" : "data/student_portal/app_files/user_profile_pictures/",
  "ApiUrl": "data/feed.json",
  "PhotoUrl": "data/photos.json",
  "CommentUrl": "data/comments.json",
  "FriendsUrl": "data/friends.json",
  "MessagesUrl": "data/messages.json",
  "MessageUrl": "data/message.json",
  "ProductUrl": "data/products.json",
  "WordPress": "http://tirana.turgutozal.edu.al/",
});
// config contact
app.constant("ConfigContact", {
  "EmailId": "weblogtemplatesnet@gmail.com",
  "ContactSubject": "Contact"
});
// config admon
app.constant("ConfigAdmob", {
  "interstitial": "ca-app-pub-3940256099942544/1033173712",
  "banner": "ca-app-pub-3940256099942544/6300978111"
});
// push notification
app.constant("PushNoti", {
  "senderID": "769158023287"
});
app.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
});
