// Ionic Starter App
var idToken,
    userId,
    serverPath,
    url,
    modalConfirmLogout,
    modalChangePassword,
    modalChangeStudentPassword,
    modalChangeStaffPassword,
    modalAssignment,
    modalVcdAssignment,
    modalEditAssignment,
    modalEditVcdAssignment,
    modalAddIncome,
    modalAddOutcome,
    modalAddIncomeAdmin,
    modalAddOutcomeAdmin,
    studentId,
    staffId,
    class_id,
    course_id,
    class_id_vcd,
    course_id_vcd,
    assignmentId;

var app = angular.module('YourApp', ['ionic', 'templates', 'ngSanitize', 'ngCordova', 'ngIOS9UIWebViewPatch', 'mobsocial.products']);

// not necessary for a web based app // needed for cordova/ phonegap application
app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // Set the statusbar to use the default style, tweak this to
            // remove the status bar on iOS or change it to use white instead of dark colors.
            StatusBar.styleDefault();
        }
    });
});

//app run getting device id
app.run(function ($rootScope, myPushNotification, $log) {
    // app device ready
    document.addEventListener("deviceready", function () {
        if (!localStorage.device_token_syt || localStorage.device_token_syt == '-1') {
            myPushNotification.registerPush();
        }
    });
    $rootScope.get_device_token = function () {
        if (localStorage.device_token_syt) {
            $log.log(localStorage.device_token_syt);
            return localStorage.device_token_syt;
        } else {
            return '-1';
        }
    }
    $rootScope.set_device_token = function (token) {
        localStorage.device_token_syt = token;
        $log.log(localStorage.device_token_syt);
        return localStorage.device_token_syt;
    }
});

//myservice device registration id to localstorage
app.service('myService', ['$http', '$log', function ($http, $log) {
    this.registerID = function (regID, platform) {
        $log.log(regID);
        localStorage.device_token_syt = regID;
    }
}]);

app.directive('clickForOptionsWrapper', [function() {
    return {
        restrict: 'A',
        controller: function($scope) {
            this.closeOptions = function() {
                $scope.$broadcast('closeOptions');
            }
        }
    };
}]);

app.directive('clickForOptions', ['$ionicGesture', function($ionicGesture) {
        return {
            restrict: 'A',
            scope: false,
            require: '^clickForOptionsWrapper',
            link: function (scope, element, attrs, parentController) {
                // A basic variable that determines wether the element was currently clicked
                var clicked;

                // Set an initial attribute for the show state
                attrs.$set('optionButtons', 'hidden');

                // Grab the content
                var content = element[0].querySelector('.item-content');

                // Grab the buttons and their width
                var buttons = element[0].querySelector('.item-options');

                var closeAll = function() {
                    element.parent()[0].$set('optionButtons', 'show');
                };

                // Add a listener for the broadcast event from the parent directive to close
                var previouslyOpenedElement;
                scope.$on('closeOptions', function() {
                    if (!clicked) {
                        attrs.$set('optionButtons', 'hidden');
                    }
                });

                // Function to show the options
                var showOptions = function() {
                    // close all potentially opened items first
                    parentController.closeOptions();

                    var buttonsWidth = buttons.offsetWidth;
                    ionic.requestAnimationFrame(function() {
                        // Add the transition settings to the content
                        content.style[ionic.CSS.TRANSITION] = 'all ease-out .25s';

                        // Make the buttons visible and animate the content to the left
                        buttons.classList.remove('invisible');
                        content.style[ionic.CSS.TRANSFORM] = 'translate3d(-' + buttonsWidth + 'px, 0, 0)';

                        // Remove the transition settings from the content
                        // And set the "clicked" variable to false
                        setTimeout(function() {
                            content.style[ionic.CSS.TRANSITION] = '';
                            clicked = false;
                        }, 250);
                    });
                };

                // Function to hide the options
                var hideOptions = function() {
                    var buttonsWidth = buttons.offsetWidth;
                    ionic.requestAnimationFrame(function() {
                        // Add the transition settings to the content
                        content.style[ionic.CSS.TRANSITION] = 'all ease-out .25s';

                        // Move the content back to the original position
                        content.style[ionic.CSS.TRANSFORM] = '';

                        // Make the buttons invisible again
                        // And remove the transition settings from the content
                        setTimeout(function() {
                            buttons.classList.add('invisible');
                            content.style[ionic.CSS.TRANSITION] = '';
                        }, 250);
                    });
                };

                // Watch the open attribute for changes and call the corresponding function
                attrs.$observe('optionButtons', function(value){
                    if (value == 'show') {
                        showOptions();
                    } else {
                        hideOptions();
                    }
                });

                // Change the open attribute on tap
                $ionicGesture.on('tap', function(e){
                    clicked = true;
                    if (attrs.optionButtons == 'show') {
                        attrs.$set('optionButtons', 'hidden');
                    } else {
                        attrs.$set('optionButtons', 'show');
                    }
                }, element);
            }
        };
    }]);

// config to disable default ionic navbar back button text and setting a new icon
// logo in back button can be replaced from /templates/sidebar-menu.html file
app.config(function ($ionicConfigProvider) {
    $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-back').previousTitleText(false);
    $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.views.maxCache(5);
});

// intro controller //
app.controller('IntroCtrl', ['$scope', '$state', '$ionicSlideBoxDelegate', function ($scope, $state, $ionicSlideBoxDelegate) {
    // Called to navigate to the main app
    $scope.next = function () {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function () {
        $ionicSlideBoxDelegate.previous();
    };

    // Called each time the slide changes
    $scope.slideChanged = function (index) {
        $scope.slideIndex = index;
    };
    // discard and move to homepage
    $scope.discardIntroPage = function () {
        $state.go('login');
    }
}]);

/**
 * Main controller
 *
 * Generate menu data if user is logged in successfully or if userId exists otherwise go to intro -> login
 * Get profile data if user is logged in
 */

app.controller('MainCtrl', function ($scope, $ionicSideMenuDelegate, $ionicModal, $ionicHistory, $ionicPopup, $state, $log, $ionicLoading, MenuData, ProfileData, Config, AuthService, AUTH_EVENTS) {

    // get token and userId
    idToken = AuthService.token();
    userId = AuthService.getuserid();

    // response data
    console.group('Main Section');
    $log.log('User Id: '+userId);
    $log.log('Token: '+idToken);
    console.groupEnd();

    $ionicHistory.nextViewOptions({
        disableBack: true
    });

    // check tokenId
    if (idToken == null || idToken === null || idToken == '') {
        $state.go('intro', {}, {reload: true});
    } else {

        /**
         *  Generate menu data
         */
        MenuData.menuItems(idToken, userId)
            .success(function (data) {
                //hide loader
                $ionicLoading.hide();
                // get menu sidemenu
                $scope.menuItems = data;
                $scope.menuItems.submenu = data.submenu;
                // response data
                console.group('Menu Data Section');
                $log.log(data);
                console.groupEnd();
            })
            .error(function (error) {
                //hide loader
                $ionicLoading.hide();
                // alert notification where something went wrong
                var alertPopup = $ionicPopup.alert({
                    title: 'Something went worng!',
                    template: error
                });
            });

        /**
         *  Get profile data
         */
        ProfileData.profileData(idToken, userId)
            .success(function (data) {
                //hide loader
                $ionicLoading.hide();
                $scope.$root.profileData = data;
                $scope.$root.photo = Config.ApiTokUrl + Config.StuffProfileImgUrl + data.photo;
                console.group('Profile Data Section');
                $log.log(data);
                console.groupEnd();
            })
            .error(function (error) {
                //hide loader
                $ionicLoading.hide();
                // alert notification where something went wrong
                var alertPopup = $ionicPopup.alert({
                    title: 'Something went worng!',
                    template: error
                });
            });
    }

    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */
    $scope.toggleGroup = function (group) {
        group.show = !group.show;
    };
    $scope.isGroupShown = function (group) {
        return group.show;
    };

    // Logout modal
    $ionicModal.fromTemplateUrl('templates/general/modal/confirm_logout.html', function (modal1) {
        modalConfirmLogout = modal1;
    }, {
        animation: 'slide-in-up',
        focusFirstInput: true
    });

    // Change password modal
    $ionicModal.fromTemplateUrl('templates/general/modal/change_password.html', function (modal1) {
        modalChangePassword = modal1;
    }, {
        animation: 'slide-in-up',
        focusFirstInput: true
    });

    // Change student password modal
    $ionicModal.fromTemplateUrl('templates/general/modal/change_student_password.html', function (modal1) {
        modalChangeStudentPassword = modal1;
    }, {
        animation: 'slide-in-up',
        focusFirstInput: true
    });

    // Change student password modal
    $ionicModal.fromTemplateUrl('templates/general/modal/change_staff_password.html', function (modal1) {
        modalChangeStaffPassword = modal1;
    }, {
        animation: 'slide-in-up',
        focusFirstInput: true
    });

    // Add assignment on selected class
    $ionicModal.fromTemplateUrl('templates/general/modal/add_assignment.html', function (modal1) {
        modalAssignment = modal1;
    }, {
        animation: 'slide-in-up',
        focusFirstInput: true
    });

    // Add assignment on selected class
    $ionicModal.fromTemplateUrl('templates/general/modal/add_vcd_assignment.html', function (modal1) {
        modalVcdAssignment = modal1;
    }, {
        animation: 'slide-in-up',
        focusFirstInput: true
    });

    // Open password modal
    $scope.openModalPassword = function () {
        modalChangePassword.show();
    };

    // Open student password modal
    $scope.openModalStudentPassword = function (stId) {
        studentId = stId;
        $log.log('StudentId :' + studentId);
        modalChangeStudentPassword.show();
    };
    $scope.openModalStaffPassword = function (stId) {
        staffId = stId;
        $log.log('StaffId :' + staffId);
        modalChangeStaffPassword.show();
    };

    // open confrim logout modal
    $scope.openModalConfirmLogout = function () {
        modalConfirmLogout.show();
    };

    // open add Assignment modal
    $scope.openModalAssignment = function () {
        modalAssignment.show();
    };

    // open add Assignment modal
    $scope.openVcdModalAssignment = function () {
        modalVcdAssignment.show();
    };

    $scope.username = AuthService.username();

    $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
        AuthService.logout();
        $state.go('intro', {}, {reload: true});
        $ionicLoading.show({template: 'Session Lost! Sorry, You have to login again.', duration: 1500});
    });

    $scope.setCurrentUsername = function (name) {
        $scope.username = name;
    };

    // Toggle left function for app sidebar
    $scope.toggleLeft = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };
    // go back to previous page
    $scope.goBackOne = function () {
        $ionicHistory.goBack();
    };
    // sharing plugin
    /*$scope.shareMain = function () {
     var title = "Download Smove For Android";
     var url = "https://play.google.com/store/apps/details?id=com.myspecialgames.swipe";
     window.plugins.socialsharing.share(title, null, null, url)
     };
     $scope.shareArticle = function (title, url) {
     window.plugins.socialsharing.share(title, null, null, url)
     };
     $scope.openLinkArticle = function (url) {
     window.open(url, '_system');
     }*/
});

/**
 * Login controller, after success login go to dashboard
 */
app.controller('LoginCtrl', function ($scope, $http, $state, $ionicViewService, $ionicHistory, $ionicLoading, $log, $ionicPopup, LoginData, AuthService) {

    $scope.doLogin = function (userData) {
        // show loader
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        /**
         * Login module
         */
        LoginData.doLogin(userData.username, userData.password)
            .success(function (data) {
                //hide loader
                $ionicLoading.hide();
                if (data.token && data.error != true) {
                    // split data , to userId and Token
                    userId = data.token.split(".")[0];
                    idToken = data.token.split(".")[1];

                    // store user data
                    AuthService.login(userData.username, userData.password, idToken, userId);
                    $state.go('app.dashboard', {}, {reload: true});
                    $ionicViewService.nextViewOptions({
                        disableBack: false
                    });
                    $scope.setCurrentUsername(userData.username);

                    // response data
                    console.group('Login Section');
                    $log.log('User role: '+data.role);
                    $log.log('User Id: '+userId);
                    $log.log('User token: '+idToken);
                    $log.log(data);
                    console.groupEnd();

                } else {
                    //hide loader
                    $ionicLoading.hide();
                    // alert notification where something went wrong
                    var alertPopup = $ionicPopup.alert({
                        title: 'Login failed!',
                        template: data.message
                    });
                }
            })
            .error(function (error) {
                //hide loader
                $ionicLoading.hide();
                // alert notification where something went wrong
                var alertPopup = $ionicPopup.alert({
                    title: 'Something went worng!',
                    template: error
                });
            });
    }
});

/**
 * Logout controller
 */
app.controller('LogoutCtrl', function ($scope, $http, $state, $ionicViewService, $ionicHistory, $ionicLoading, $ionicPopup, LoginData, AuthService) {

    $scope.closeModalConfirmLogout = function () {
        modalConfirmLogout.hide();
    };
    /**
     * Logout function
     */
    $scope.logout = function () {
        modalConfirmLogout.hide();
        AuthService.logout();
        $state.go('login');
        $ionicLoading.show({template: 'We hope to see you again', duration: 1500});
    };
});

/**
 * Profile controller, get user data if logged in successfully
 */
app.controller('ProfileCtrl', function ($scope, $cordovaCamera, $cordovaFile, $ionicActionSheet, $ionicLoading, $ionicPopup, $log, $ionicModal, ProfileData, Config, ChangeProfileData) {

    $scope.listCanSwipe = true;
    $scope.gender = [
        {text: "Male", value: "m"},
        {text: "Female", value: "f"}
    ];

    // show loader
    $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
    });

    /**
     * Get profile data
     */
    ProfileData.profileData(idToken, userId)
        .success(function (data) {
            //hide loader
            $ionicLoading.hide();
            $scope.profileData = data;
            $scope.profileName = data.name;
            $scope.profileSurname = data.surname;
            $scope.photo = Config.ApiTokUrl + Config.StuffProfileImgUrl + data.photo;

            $scope.$root.profileData = data;
            $scope.$root.photo = Config.ApiTokUrl + Config.StuffProfileImgUrl + data.photo;

            console.group('Profile Data Section');
            $log.log(data);
            console.groupEnd();
        })
        .error(function (error) {
            //hide loader
            $ionicLoading.hide();
            // alert notification where something went wrong
            var alertPopup = $ionicPopup.alert({
                title: 'Something went worng!',
                template: error
            });
        });

    // Modular popup to change data profile
    $scope.showPopup = function (title, type, db_field_name, db_field_name1, db_field_name2, value, value1, value2) {

        // manipulate data
        $scope.data = {};
        $scope.data.inputValue = value;
        $scope.data.inputValue1 = value1;
        $scope.data.inputValue2 = value2;
        $scope.data.type = type;
        $scope.data.setGender = value;

        // on dataChange ( gender : radio button )
        $scope.dataChange = function (item) {
            $scope.data.inputValue = item.value;
        };

        // show popup with current editable input field
        var myPopup = $ionicPopup.show({
            templateUrl: 'templates/general/popup/editUserData.html',
            title: title,
            scope: $scope,
            buttons: [
                {text: 'Cancel'},
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        if (!$scope.data.inputValue) {
                            //don't allow the user to close unless he enters input values
                            e.preventDefault();
                        } else {

                            // show loader
                            $ionicLoading.show({
                                content: 'Loading',
                                animation: 'fade-in',
                                showBackdrop: true,
                                maxWidth: 200,
                                showDelay: 0
                            });

                            /**
                             * Change profile data module
                             */
                            ChangeProfileData.response(idToken, userId,  db_field_name, db_field_name1, db_field_name2, $scope.data.inputValue, $scope.data.inputValue1, $scope.data.inputValue2)
                                .success(function (data) {
                                    //hide loader
                                    $ionicLoading.show({template: data.message, duration: 1500});

                                    // check db field name, based on that set current value
                                    if (db_field_name == 'name') {
                                        $scope.profileData.name = $scope.data.inputValue;
                                        $scope.profileData.middle_name = $scope.data.inputValue1;
                                        $scope.profileData.surname = $scope.data.inputValue2;
                                    } else if (db_field_name == 'gender') {
                                        $scope.data.setGender = $scope.data.inputValue;
                                        $scope.profileData.gender = $scope.data.inputValue;
                                    } else if (db_field_name == 'birth_place') {
                                        $scope.profileData.birth_place = $scope.data.inputValue;
                                    } else if (db_field_name == 'CITIZENSHIP') {
                                        $scope.profileData.CITIZENSHIP = $scope.data.inputValue;
                                    } else if (db_field_name == 'mobile_phone') {
                                        $scope.profileData.mobile_phone = $scope.data.inputValue;
                                    } else if (db_field_name == 'home_phone') {
                                        $scope.profileData.home_phone = $scope.data.inputValue;
                                    } else if (db_field_name == 'education') {
                                        $scope.profileData.education = $scope.data.inputValue;
                                    } else if (db_field_name == 'university') {
                                        $scope.profileData.university = $scope.data.inputValue;
                                    } else if (db_field_name == 'faculty') {
                                        $scope.profileData.faculty = $scope.data.inputValue;
                                    } else if (db_field_name == 'department') {
                                        $scope.profileData.department = $scope.data.inputValue;
                                    } else if (db_field_name == 'graduation_year') {
                                        $scope.profileData.graduation_year = $scope.data.inputValue;
                                    } else if (db_field_name == 'residence') {
                                        $scope.profileData.residence = $scope.data.inputValue;
                                    } else if (db_field_name == 'birthday') {
                                        $scope.profileData.birthday = $scope.data.inputValue;
                                    }
                                })
                                .error(function (error) {
                                    //hide loader
                                    $ionicLoading.hide();
                                    // alert notification where something went wrong
                                    var alertPopup = $ionicPopup.alert({
                                        title: 'Something went worng!',
                                        template: error
                                    });
                                });
                        }
                    }
                }
            ]
        });
    };

    /**
     * Change profile picture by uploading a new one ( from camera or gallery images)
     */
    $scope.showActionUploadImage  = function () {

        $ionicActionSheet.show({
            titleText: 'Select from :',
            buttons: [
                {text: '<i class="icon ion-camera"></i> Camera'},
                {text: '<i class="icon ion-images"></i> Gallery'}
            ],
            cancelText: 'Cancel',
            destructiveText: 'Cancel',
            cancel: function () {
                $log.log('CANCELLED');
            },
            buttonClicked: function (index) {
                switch (index){
                    case 0 :
                        takePicture();
                        return true;
                    case 1 :
                        selectPicture();
                        return true;
                }
            },
            destructiveButtonClicked: function () {
                $log.log('DESTRUCT');
                return true;
            }
        });
    };

    $scope.data = {"ImageURI": "Select Image"};

    /**
     *  On taking picture by camera
     */
    var takePicture = function () {

        // specify option
        var options = {
            quality: 100,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 150,
            targetHeight: 150,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(
            function (imageData) {
                $scope.data.imgUrl = imageData;
                $scope.ftLoad = true;
                $scope.success = true;
                uploadPicture(); // update image on backend
            },
            function (err) {
                $ionicLoading.show({template: 'Something went wrong on uploading picture', duration: 500});
            });
    };

    /**
     *  On getting picture by Photogallery
     */
    var selectPicture = function () {

        // specify option
        var options = {
            quality: 50,
            targetWidth: 150,
            targetHeight: 150,
            allowEdit:true,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: Camera.EncodingType.JPEG
        };

        $cordovaCamera.getPicture(options).then(
            function (imageURI) {
                $scope.data.imgUrl = imageURI;
                $scope.success = true;
                uploadPicture(); // update image on backend
            },
            function (err) {
                $ionicLoading.show({template: 'Something went wrong on getting image', duration: 500});
            })
    };

    /**
     * Upload profile picture
     */
    var uploadPicture = function () {
        // show loader
        $ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        // specify options
        var fileURL = $scope.data.imgUrl;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.chunkedMode = true;

        var params = {};
        params.value1 = $scope.profileName;
        params.value2 = $scope.profileSurname;

        options.params = params;

        // prepare and do upload
        var ft = new FileTransfer();
        ft.upload(fileURL,Config.ApiTokUrl+'General/upload?sid='+idToken+'&userId='+userId, uploadSuccess, uploadError, options);

        // On upload success
        function uploadSuccess(r) {
            $ionicLoading.hide();
            var response = JSON.parse(r.response);
            console.group('Upload Image Section');
            $log.log('Upload Status: '+ response.error);
            console.groupEnd();
            $ionicLoading.show({template: response.message, duration: 1500});
        }

        // On upload error
        function uploadError(error) {
            $ionicLoading.show({template: 'Something went wrong on uploading your image , please try again later.', duration: 1500});
        }
    };
});

/**
 * Dashboard Controller
 * Todo: Add some stuff here
 */
app.controller('DashboardCtrl', function ($scope, $ionicActionSheet, $log) {

    // Simple Action sheet test,Triggered on a button click, or some other target
    $scope.showActionsheet = function () {

        $ionicActionSheet.show({
            titleText: 'ActionSheet Example',
            buttons: [
                {text: '<i class="icon ion-share"></i> Share'},
                {text: '<i class="icon ion-arrow-move"></i> Move'},
            ],
            destructiveText: 'Delete',
            cancelText: 'Cancel',
            cancel: function () {
                $log.log('CANCELLED');
            },
            buttonClicked: function (index) {
                $log.log('BUTTON CLICKED', index);
                return true;
            },
            destructiveButtonClicked: function () {
                $log.log('DESTRUCT');
                return true;
            }
        });
    };
});

/**
 * Change password
 */
app.controller('ChangePasswordCtrl', function ($scope, $http, $state, $ionicViewService, $ionicLoading, $log, $ionicPopup, ChangePassword) {

    $scope.closeModalPassword = function () {
        modalChangePassword.hide();
    };

    $scope.changePassword = function (passwordData) {
        if (passwordData.new_password != passwordData.retype_password) {
            var alertPopup = $ionicPopup.alert({
                title: '',
                template: 'Your new password doesn`t match'
            });
        } else if (passwordData.new_password == '' || passwordData.retype_password == '' || passwordData.old_password == '') {
            var alertPopup = $ionicPopup.alert({
                title: 'Validation Error',
                template: 'Fields are required'
            });
        } else {
            // show loader
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            ChangePassword.response(idToken, userId, passwordData.new_password, passwordData.old_password)
                .success(function (data) {
                    //hide loader
                    $ionicLoading.hide();
                    // Show notification where something went wrong
                    if (data.status == 'success') {
                        modalChangePassword.hide();
                    }
                    $ionicLoading.show({template: data.message, duration: 1500});

                    console.group('Change Password Section');
                    $log.log(data);
                    console.groupEnd();
                })
                .error(function (error) {
                    //hide loader
                    $ionicLoading.hide();
                    // alert notification where something went wrong
                    var alertPopup = $ionicPopup.alert({
                        title: 'Something went worng!',
                        template: error
                    });
                });
        }
    }
});

/**
 * Change Student password
 */
app.controller('ChangeStudentPasswordCtrl', function ($scope, $http, $state, $ionicViewService, $ionicLoading, $log, $ionicPopup, ChangeStudentPassword) {

    $scope.closeModalStudentPassword = function () {
        modalChangeStudentPassword.hide();
    };

    /**
     * Function changeStudentPassword , change students password
     * @param passwordData
     */
    $scope.changeStudentPassword = function (passwordData) {
        if (passwordData.new_password == '') {
            $ionicLoading.show({template: 'Please enter new password', duration: 1500});
        } else {
            // show loader
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            ChangeStudentPassword.response(idToken, userId, studentId, passwordData.new_password)
                .success(function (data) {
                    //hide loader
                    $ionicLoading.hide();
                    // alert notification where something went wrong
                    if (data.status == 'success') {
                        modalChangeStudentPassword.hide();
                    }
                    $ionicLoading.show({template: data.message, duration: 1500});

                    console.group('Change Student Password Section');
                    $log.log(data);
                    console.groupEnd();
                })
                .error(function (error) {
                    //hide loader
                    $ionicLoading.hide();
                    // alert notification where something went wrong
                    var alertPopup = $ionicPopup.alert({
                        title: 'Something went wrong!',
                        template: error
                    });
                });
        }
    }
});