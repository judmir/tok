angular.module('YourApp')

    .service('AuthService', function ($q, $http) {

        // declare variable
        var LOCAL_TOKEN_KEY = 'A-report-token';
        var USER_ID = 'User-Id';
        var username = '';
        var isAuthenticated = false;
        var role = '';
        var authToken;
        var storetoken;
        var userId;

        // load user credentials
        function loadUserCredentials() {
            var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            var user_d = window.localStorage.getItem(USER_ID);
            if (token) {
                //call function
                useCredentials(token, user_d);
            }
        }

        // Store user credentials on local Storage (store token )
        function storeUserCredentials(token, userId) {
            window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
            window.localStorage.setItem(USER_ID, userId);
            useCredentials(token, userId);
        }

        // get user from token
        function useCredentials(token, user_id) {
            isAuthenticated = true;
            authToken = token;
            storetoken = token;
            userId = user_id;
            // Set the token as header for your requests!
            $http.defaults.headers.common['X-Auth-Token'] = token;
            $http.defaults.headers.common['User-Id'] = user_id;
        }

        // destroy user credentials , remove user token from local storage
        function destroyUserCredentials() {
            authToken = undefined;
            username = '';
            isAuthenticated = false;
            storetoken = undefined;
            userId = undefined;
            $http.defaults.headers.common['X-Auth-Token'] = undefined;
            $http.defaults.headers.common['User-Id'] = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
            window.localStorage.removeItem(USER_ID);
        }

        // Login function
        var login = function (name, pw, token, userId) {
            return $q(function (resolve, reject) {
                // Make a request and receive your auth token from your server
                storeUserCredentials(token, userId);
                resolve('Login success.');
            });
        };

        // Logout function
        var logout = function ($state) {
            destroyUserCredentials();
        };

        loadUserCredentials();

        return {
            login: login,
            logout: logout,
            path: function () { return serverPath},
            token: function () { return storetoken;},
            getuserid: function () { return userId;},
            username: function () { return username; },
            role: function () { return role; }
        };
    })
    //check the authenticated and authorized events
    .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
        return {
            responseError: function (response) {
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized
                }[response.status], response);
                return $q.reject(response);
            }
        };
    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });
